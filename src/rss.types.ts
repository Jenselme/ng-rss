import { Observable } from 'rxjs';

export interface Options {
  fetchDescriptions?: boolean;
  onlyUnread?: boolean;
  readOnScroll?: boolean;
}

export type RssBackends = 'TTRSS';

export type AuthValues = {
  host: string;
  username: string;
  password: string;
  rssBackend: RssBackends;
  token: string;
};

export type CategoryId = string;

export interface Category {
  readonly id: CategoryId;
  readonly title: string;
  readonly unreadCount: number;
  readonly isSpecial: boolean;
}

export type Counter = Record<CategoryId, number>;

export type FeedId = number | string;
export type ArticleId = number | string;

export interface Article {
  readonly author: string;
  readonly feedId: FeedId;
  readonly feedTitle: string;
  readonly id: ArticleId;
  readonly isRead: boolean;
  readonly isFavorite: boolean;
  readonly link: string;
  readonly title: string;
  readonly description: string;
  readonly updatedAt: Date;
}

export interface RSSBackend {
  login(values: Omit<AuthValues, 'token'>): Observable<AuthValues>;
  logout(): Observable<void>;
  isLoggedIn(): Observable<boolean>;
  listCategories(): Observable<Category[]>;
  listCounters(): Observable<Counter>;
  updateCategoriesWithCounters(categories: Category[], counters: Counter): Category[];
  listArticles(categoryId: CategoryId): Observable<Article[]>;
  markArticlesAsRead(articleIds: ArticleId[]): Observable<void>;
  markArticlesAsUnread(articleIds: ArticleId[]): Observable<void>;
  markArticleAsFavorite(articleId: ArticleId): Observable<void>;
  markArticleAsNotFavorite(articleId: ArticleId): Observable<void>;
  transformCategoryUrlIdToCategoryId(categoryUrlId: string): CategoryId;
}

export type ArticlesData = {
  articles: Article[];
  loading: boolean;
};

export type CategoriesData = {
  activeCategoryId: CategoryId | undefined;
  categories: Category[];
  loading: boolean;
};

export interface MainRssBackend {
  get categories(): Observable<CategoriesData>;
  get articles(): Observable<ArticlesData>;
  login(values: Omit<AuthValues, 'token'>): Observable<AuthValues>;
  logout(): Observable<void>;
  isLoggedIn(): Observable<boolean>;
  listCategories(): void;
  changeActiveCategory(categoryId: CategoryId): void;
  refreshCurrentCategory(): void;
  listArticlesInCurrentCategory(): void;
  markArticlesAsRead(articles: Article[]): void;
  markArticleAsReadAndDiscard(article: Article): void;
  markArticlesAsUnread(articles: Article[]): void;
  markArticleAsFavorite(article: Article): void;
  markArticleAsNotFavorite(article: Article): void;
  transformCategoryUrlIdToCategoryId(categoryUrlId: string): CategoryId;
}
