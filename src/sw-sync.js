(function () {
  // Keep in sync with config.ts
  const swActionSyncName = 'ngrss';
  const swDynamicDbName = 'ngrss';
  const swDynamicDbSchemaVersion = 1;

  importScripts('dexie.min.js');

  const db = new Dexie(swDynamicDbName);
  db.version(swDynamicDbSchemaVersion).stores({
    ttrss: 'id,url,payload,failureCounts',
  });

  self.addEventListener('sync', (event) => {
    console.log(event);
    if (event.tag !== swActionSyncName) {
      return;
    }

    event.waitUntil(syncTTRSSActions());
  });

  const syncTTRSSActions = async () => {
    const processedActionIds = [];
    const failedActionIds = [];
    const actions = await db.ttrss.toArray();
    const syncActions = actions.map((action) =>
      fetch(action.url, {
        body: JSON.stringify(action.payload),
        method: 'POST',
      }).then(
        () => processedActionIds.push(action.id),
        (error) => {
          console.error(`Failed to sync action ${action.id}: ${error}`);
          failedActionIds.push(action.id);
        },
      ),
    );

    await Promise.all(syncActions);
    await db.ttrss.where('id').anyOf(processedActionIds).delete();
    await db.ttrss
      .where('id')
      .anyOf(failedActionIds)
      .modify((action) => (action.failureCounts += 1));
    await db.ttrss.where('failureCounts').above(4).delete();
  };
})();
