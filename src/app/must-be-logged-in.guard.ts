import { CanActivateFn, Router } from '@angular/router';
import { RSSService } from './rssservice.service';
import { inject } from '@angular/core';
import { of, switchMap } from 'rxjs';

export const mustBeLoggedInGuard: CanActivateFn = () => {
  const router = inject(Router);
  const rssBackend = inject(RSSService);

  return rssBackend.isLoggedIn().pipe(
    switchMap((isLoggedIn) => {
      if (isLoggedIn) {
        return of(true);
      }

      return of(router.parseUrl('/'));
    }),
  );
};
