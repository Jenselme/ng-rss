import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RssBackends } from '../../rss.types';
import { urlValidator } from '../validators';
import { RSSService } from '../rssservice.service';
import { first, Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { StorageService } from '../storage.service';
import { Title } from '@angular/platform-browser';
import { TranslocoService } from '@ngneat/transloco';

interface LoginForm {
  host: FormControl<string>;
  username: FormControl<string>;
  password: FormControl<string>;
  rssBackend: FormControl<RssBackends>;
}

@Component({
  selector: 'rss-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css'],
})
export class LoginViewComponent implements OnInit, OnDestroy {
  private translationEventsSubscription: Subscription | undefined;
  loginSubscription: Subscription | undefined;
  authDataSubscription: Subscription | undefined;
  loggingIn = false;

  loginForm = new FormGroup<LoginForm>({
    host: new FormControl('', {
      nonNullable: true,
      validators: [Validators.required, urlValidator],
    }),
    username: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    password: new FormControl('', { nonNullable: true, validators: [Validators.required] }),
    rssBackend: new FormControl('TTRSS', { nonNullable: true, validators: [Validators.required] }),
  });

  constructor(
    private title: Title,
    private translocoService: TranslocoService,
    private rssService: RSSService,
    private snackbar: MatSnackBar,
    private router: Router,
    private storage: StorageService,
  ) {}

  ngOnInit() {
    this.translationEventsSubscription = this.translocoService
      .selectTranslate('login.page-title')
      .subscribe((title) => this.title.setTitle(title));

    this.authDataSubscription = this.storage.authValues
      .pipe(first())
      .subscribe((existingAuthData) => {
        if (!existingAuthData) {
          return;
        }

        this.loginForm.setValue({
          host: existingAuthData.host,
          username: existingAuthData.username,
          password: existingAuthData.password,
          rssBackend: existingAuthData.rssBackend,
        });
      });
  }

  ngOnDestroy() {
    this.translationEventsSubscription?.unsubscribe();
    this.loginSubscription?.unsubscribe();
    this.authDataSubscription?.unsubscribe();
  }

  login() {
    if (!this.loginForm.valid) {
      return;
    }

    this.loggingIn = true;
    this.loginSubscription = this.rssService.login(this.loginForm.getRawValue()).subscribe({
      complete: () => {
        this.loggingIn = false;
        this.router.navigate(['/categories']);
      },
      error: () => {
        this.snackbar.open('Failed to login with provided credentials', undefined, {
          duration: 3_000,
          panelClass: ['red-snackbar'],
        });
        this.loggingIn = false;
      },
    });
  }
}
