import { Component, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RSSService } from '../rssservice.service';
import { filter, interval, of, Subscription, switchMap } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TranslocoService } from '@ngneat/transloco';
import { MatDialog } from '@angular/material/dialog';
import { OptionsDialogComponent } from '../options-dialog/options-dialog.component';
import { CategoriesDialogComponent } from '../categories-dialog/categories-dialog.component';
import { Config, CONFIG } from '../../config';
import { Category } from '../../rss.types';
import { Platform } from '@angular/cdk/platform';

@Component({
  selector: 'rss-articles-view',
  templateUrl: './articles-view.component.html',
  styleUrls: ['./articles-view.component.css'],
})
export class ArticlesViewComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription | undefined;
  private refreshSubscription: Subscription | undefined;
  private updateTitleSubscription: Subscription | undefined;
  private mustRefreshOnWindowFocus = false;
  private isWindowActive = true;
  activeCategory: Category | undefined;

  constructor(
    @Inject(CONFIG) private config: Config,
    private title: Title,
    private dialog: MatDialog,
    private translocoService: TranslocoService,
    private rssBackend: RSSService,
    private route: ActivatedRoute,
    private platform: Platform,
  ) {}

  ngOnInit() {
    this.refreshSubscription = interval(this.config.refreshInterval).subscribe(() => {
      if (!this.isWindowActive && (this.platform.ANDROID || this.platform.IOS)) {
        this.mustRefreshOnWindowFocus = true;
      } else {
        this.refreshArticlesList();
      }
    });

    this.routeParamsSubscription = this.route.params.subscribe((params) => {
      this.rssBackend.changeActiveCategory(params['id']);
      this.rssBackend.listCategories();
    });

    this.updateTitleSubscription = this.rssBackend.categories
      .pipe(
        filter(({ activeCategoryId, categories }) => !!activeCategoryId && categories.length > 0),
      )
      .pipe(
        switchMap(({ activeCategoryId, categories }) => {
          this.activeCategory = categories.find((category) => category.id === activeCategoryId);
          if (!this.activeCategory) {
            return of('');
          } else if (!this.activeCategory.isSpecial) {
            return of(this.activeCategory.title);
          }

          return this.translocoService.selectTranslate(this.activeCategory.title);
        }),
      )
      .pipe(
        switchMap((categoryTitle) => {
          if (!categoryTitle) {
            return of('');
          }

          return this.translocoService.selectTranslate('articles.page-title', {
            category: categoryTitle,
          });
        }),
      )
      .subscribe((title) => {
        if (!title) {
          return;
        }

        this.title.setTitle(title);
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription?.unsubscribe();
    this.refreshSubscription?.unsubscribe();
    this.updateTitleSubscription?.unsubscribe();
  }

  @HostListener('window:focus') onFocus() {
    this.isWindowActive = true;
    if (this.mustRefreshOnWindowFocus) {
      this.mustRefreshOnWindowFocus = false;
      this.refreshArticlesList();
    }
  }

  @HostListener('window:blur') onBlur() {
    this.isWindowActive = false;
  }

  openOptionsDialog() {
    this.dialog.open(OptionsDialogComponent);
  }

  openCategoriesDialog() {
    this.dialog.open(CategoriesDialogComponent);
  }

  private refreshArticlesList() {
    this.rssBackend.refreshCurrentCategory();
  }
}
