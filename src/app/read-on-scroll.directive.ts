import { Directive, ElementRef, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Article } from '../rss.types';
import { ReadOnScrollService } from './read-on-scroll.service';

@Directive({
  selector: '[rssReadOnScroll]',
})
export class ReadOnScrollDirective implements OnChanges, OnDestroy {
  @Input({ required: true, alias: 'rssReadOnScroll' }) article: Article | undefined;

  constructor(
    private elementRef: ElementRef,
    private readOnScroll: ReadOnScrollService,
  ) {}

  ngOnDestroy() {
    if (this.article) {
      this.readOnScroll.unregister(this.article);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.readOnScroll.register(this.elementRef.nativeElement, changes['article'].currentValue);
  }
}
