import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'rss-categories-dialog',
  templateUrl: './categories-dialog.component.html',
  styleUrls: ['./categories-dialog.component.css'],
})
export class CategoriesDialogComponent {
  constructor(private dialogRef: MatDialogRef<CategoriesDialogComponent>) {}

  close() {
    this.dialogRef.close();
  }
}
