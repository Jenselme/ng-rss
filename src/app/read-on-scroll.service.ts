import { Injectable, OnDestroy } from '@angular/core';
import { Article, ArticleId } from '../rss.types';
import { debounceTime, fromEvent, Subscription } from 'rxjs';
import { RSSService } from './rssservice.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class ReadOnScrollService implements OnDestroy {
  private registeredArticles: Map<ArticleId, { article: Article; element: HTMLElement }> =
    new Map();
  private articleIdsBeingProcessed: Set<ArticleId> = new Set();
  private readonly scrollSubscription: Subscription;

  constructor(
    private storage: StorageService,
    private rssBackend: RSSService,
  ) {
    this.scrollSubscription = fromEvent(window, 'scrollend')
      .pipe(debounceTime(500))
      .subscribe(() => {
        if (!this.storage.snapshot.options.readOnScroll) {
          return;
        }

        const articlesToMarkAsRead: Article[] = [];
        for (const { article, element } of this.registeredArticles.values()) {
          if (element.getBoundingClientRect().top > 0) {
            break;
          } else if (article.isRead || this.articleIdsBeingProcessed.has(article.id)) {
            continue;
          }

          this.articleIdsBeingProcessed.add(article.id);
          articlesToMarkAsRead.push(article);
        }

        this.rssBackend.markArticlesAsRead(articlesToMarkAsRead);
      });
  }

  ngOnDestroy() {
    this.registeredArticles.clear();
    this.scrollSubscription.unsubscribe();
  }

  register(element: HTMLElement, article: Article) {
    this.registeredArticles.set(article.id, { article, element });

    // When the article is updated, the directive will re-trigger the register process. So we can
    // hook here to know whether the article was processed.
    if (this.articleIdsBeingProcessed.has(article.id) && article.isRead) {
      this.articleIdsBeingProcessed.delete(article.id);
    }
  }

  unregister(article: Article) {
    this.registeredArticles.delete(article.id);
  }
}
