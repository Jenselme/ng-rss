import { Injectable, OnDestroy } from '@angular/core';
import {
  Article,
  ArticleId,
  ArticlesData,
  AuthValues,
  CategoriesData,
  CategoryId,
  MainRssBackend,
  RSSBackend,
  RssBackends,
} from '../rss.types';
import { TTRSSBackendService } from './ttrssbackend.service';
import { Observable, of, Subscription, switchMap, tap } from 'rxjs';
import { StorageService } from './storage.service';
import { ExtendedSet, SubscriptionsMap, chain } from 'src/utils/data-structures';
import { ArticlesSubject, getArticleIds } from 'src/utils/articles';
import { CategoriesSubject } from 'src/utils/categories';

@Injectable({
  providedIn: 'root',
})
export class RSSService implements MainRssBackend, OnDestroy {
  private listCategoriesSubscription: Subscription | undefined;
  private listCountersSubscription: Subscription | undefined;
  private listArticlesSubscription: Subscription | undefined;
  private articlesReadStatusUpdateSubscriptions = new SubscriptionsMap<ArticleId>();
  private articlesFavoriteStatusUpdateSubscriptions = new SubscriptionsMap<ArticleId>();
  private articlesConcernedByBulkUpdate = new ExtendedSet<ArticleId>();
  private bulkUpdateSubscriptions = new SubscriptionsMap<ArticleId[]>();
  private readonly backends: Record<RssBackends, RSSBackend>;
  private categoriesSubject = new CategoriesSubject();
  private articlesSubject = new ArticlesSubject();

  constructor(
    private storage: StorageService,
    ttrssBackend: TTRSSBackendService,
  ) {
    this.backends = {
      TTRSS: ttrssBackend,
    };
  }

  ngOnDestroy() {
    this.listArticlesSubscription?.unsubscribe();
    this.listCategoriesSubscription?.unsubscribe();
    this.listCountersSubscription?.unsubscribe();

    for (const subscription of chain<Subscription>(
      this.articlesReadStatusUpdateSubscriptions.values(),
      this.articlesFavoriteStatusUpdateSubscriptions.values(),
      this.bulkUpdateSubscriptions.values(),
    )) {
      subscription.unsubscribe();
    }
  }

  private get selectedBackend(): RSSBackend | undefined {
    return this.backends[this.storage.snapshot.authData.rssBackend];
  }

  get categories(): Observable<CategoriesData> {
    return this.categoriesSubject.asObservable();
  }

  get articles(): Observable<ArticlesData> {
    return this.articlesSubject.asObservable();
  }

  login(values: Omit<AuthValues, 'token'>): Observable<AuthValues> {
    return this.backends[values.rssBackend]
      .login(values)
      .pipe(tap((authValues) => this.storage.saveAuthData(authValues)));
  }

  logout(): Observable<void> {
    if (!this.selectedBackend) {
      return of(undefined);
    }

    return this.selectedBackend.logout().pipe(tap(() => this.storage.resetAuthData()));
  }

  isLoggedIn(): Observable<boolean> {
    if (!this.selectedBackend) {
      return of(false);
    }

    return this.selectedBackend.isLoggedIn().pipe(
      switchMap((isLoggedIn) => {
        if (isLoggedIn) {
          return of(true);
        } else if (
          !this.storage.snapshot.authData.host ||
          !this.storage.snapshot.authData.username ||
          !this.storage.snapshot.authData.password
        ) {
          return of(false);
        }

        return this.login(this.storage.snapshot.authData).pipe(
          switchMap((authValues) => of(!!authValues.token)),
        );
      }),
    );
  }

  listCategories(): void {
    if (!this.selectedBackend) {
      return;
    }

    this.listCategoriesSubscription?.unsubscribe();
    this.categoriesSubject.loading();
    this.listCategoriesSubscription = this.selectedBackend
      .listCategories()
      .subscribe((categories) => {
        this.categoriesSubject.setCategories(categories);
      });
  }

  changeActiveCategory(categoryId: CategoryId) {
    this.categoriesSubject.changeActiveCategory(
      this.transformCategoryUrlIdToCategoryId(categoryId),
    );
    this.listArticlesInCurrentCategory();
  }

  refreshCurrentCategory() {
    this.listArticlesInCurrentCategory();
    this.updateCategoriesCounters();
  }

  private updateCategoriesCounters() {
    if (!this.selectedBackend) {
      return;
    }

    this.listCountersSubscription?.unsubscribe();
    const backend = this.selectedBackend;
    this.listCountersSubscription = backend.listCounters().subscribe((counters) => {
      this.categoriesSubject.setCategories(
        backend.updateCategoriesWithCounters(this.categoriesSubject.snapshot.categories, counters),
      );
    });
  }

  listArticlesInCurrentCategory(): void {
    if (!this.selectedBackend || !this.categoriesSubject.snapshot.activeCategoryId) {
      return;
    }

    window.scrollTo(0, 0);
    this.listArticlesSubscription?.unsubscribe();
    this.articlesSubject.loading();
    this.listArticlesSubscription = this.selectedBackend
      .listArticles(this.categoriesSubject.snapshot.activeCategoryId)
      .subscribe((articles) => {
        this.articlesSubject.setArticles(articles);
      });
  }

  markArticlesAsRead(articles: Article[]): void {
    if (!this.selectedBackend) {
      return;
    }

    this.updateArticlesReadStatus(
      getArticleIds(articles.filter((article) => !article.isRead)),
      this.selectedBackend.markArticlesAsRead.bind(this.selectedBackend),
      this.articlesSubject.markAsRead.bind(this.articlesSubject),
    );
  }

  markArticleAsReadAndDiscard(articleToDiscard: Article) {
    if (!this.selectedBackend) {
      return;
    }

    if (articleToDiscard.isRead) {
      this.articlesSubject.discard([articleToDiscard.id]);
      return;
    }

    this.updateArticlesReadStatus(
      [articleToDiscard.id],
      this.selectedBackend.markArticlesAsRead.bind(this.selectedBackend),
      this.articlesSubject.discard.bind(this.articlesSubject),
    );
  }

  markArticlesAsUnread(articles: Article[]): void {
    if (!this.selectedBackend) {
      return;
    }

    this.updateArticlesReadStatus(
      getArticleIds(articles.filter((article) => article.isRead)),
      this.selectedBackend.markArticlesAsUnread.bind(this.selectedBackend),
      this.articlesSubject.markAsUnread.bind(this.articlesSubject),
    );
  }

  markArticleAsFavorite(articleToUpdate: Article): void {
    if (!this.selectedBackend || articleToUpdate.isFavorite) {
      return;
    }

    this.updateArticleFavoriteStatus(
      articleToUpdate.id,
      this.selectedBackend.markArticleAsFavorite.bind(this.selectedBackend),
      this.articlesSubject.markAsFavorite.bind(this.articlesSubject),
    );
  }

  markArticleAsNotFavorite(articleToUpdate: Article): void {
    if (!this.selectedBackend || !articleToUpdate.isFavorite) {
      return;
    }

    this.updateArticleFavoriteStatus(
      articleToUpdate.id,
      this.selectedBackend.markArticleAsNotFavorite.bind(this.selectedBackend),
      this.articlesSubject.markAsNotFavorite.bind(this.articlesSubject),
    );
  }

  private updateArticleFavoriteStatus(
    articleId: ArticleId,
    updateAction: (articleId: ArticleId) => Observable<void>,
    updateArticles: (articleId: ArticleId[]) => void,
  ): void {
    this.articlesFavoriteStatusUpdateSubscriptions.set(
      articleId,
      updateAction(articleId).subscribe(() => {
        updateArticles([articleId]);
        this.updateCategoriesCounters();
      }),
    );
  }

  private updateArticlesReadStatus(
    articleIds: ArticleId[],
    updateAction: (articleIds: ArticleId[]) => Observable<void>,
    updateArticles: (articlesIdToUpdate: ArticleId[]) => void,
  ): void {
    const articleIdsToUpdate = Array.from(
      new ExtendedSet(articleIds).difference(
        this.articlesConcernedByBulkUpdate,
        this.articlesReadStatusUpdateSubscriptions.keys(),
      ),
    );

    if (articleIdsToUpdate.length === 0) {
      return;
    }

    const subscription = updateAction(articleIdsToUpdate).subscribe(() => {
      updateArticles(articleIdsToUpdate);
      this.updateCategoriesCounters();

      if (articleIdsToUpdate.length === 1) {
        this.articlesReadStatusUpdateSubscriptions.delete(articleIdsToUpdate[0]);
      }
      this.articlesConcernedByBulkUpdate.deleteAll(articleIdsToUpdate);
      this.bulkUpdateSubscriptions.delete(articleIdsToUpdate);
    });
    if (articleIdsToUpdate.length === 1) {
      this.articlesReadStatusUpdateSubscriptions.set(articleIdsToUpdate[0], subscription);
    } else {
      this.articlesConcernedByBulkUpdate.addAll(articleIdsToUpdate);
      this.bulkUpdateSubscriptions.set(articleIdsToUpdate, subscription);
    }
  }

  transformCategoryUrlIdToCategoryId(categoryUrlId: string | undefined): CategoryId {
    if (!this.selectedBackend || !categoryUrlId) {
      return '';
    }

    return this.selectedBackend.transformCategoryUrlIdToCategoryId(categoryUrlId);
  }
}
