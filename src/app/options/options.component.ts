import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { getInitialOptions, StorageService } from '../storage.service';
import { Options } from '../../rss.types';
import { RSSService } from '../rssservice.service';

@Component({
  selector: 'rss-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css'],
})
export class OptionsComponent implements OnInit, OnDestroy {
  @Input({ required: false }) color: 'white' | '' = '';

  private optionsSubscription: Subscription | undefined;
  options: Options = getInitialOptions();

  constructor(
    private storage: StorageService,
    private rssBackend: RSSService,
  ) {}

  ngOnInit() {
    this.optionsSubscription = this.storage.options.subscribe((options) => {
      this.options = options;
    });
  }

  ngOnDestroy() {
    this.optionsSubscription?.unsubscribe();
  }

  toggleFetchDescriptions() {
    const newValue = !this.options.fetchDescriptions;
    this.storage.saveOptions({
      ...this.options,
      fetchDescriptions: newValue,
    });

    if (newValue) {
      this.rssBackend.listArticlesInCurrentCategory();
    }
  }

  toggleOnlyUnread() {
    this.storage.saveOptions({
      ...this.options,
      onlyUnread: !this.options.onlyUnread,
    });
    this.rssBackend.listArticlesInCurrentCategory();
  }

  toggleReadOnScroll() {
    this.storage.saveOptions({
      ...this.options,
      readOnScroll: !this.options.readOnScroll,
    });
  }
}
