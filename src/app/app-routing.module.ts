import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { mustBeLoggedInGuard } from './must-be-logged-in.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./login.module').then((m) => m.LoginModule),
  },
  { path: 'categories', redirectTo: 'categories/unread', pathMatch: 'full' },
  {
    path: 'categories',
    canActivate: [mustBeLoggedInGuard],
    loadChildren: () => import('./articles.module').then((m) => m.ArticlesModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
