import { Injectable } from '@angular/core';
import { Article, AuthValues, Category, CategoryId, Counter, RSSBackend } from '../rss.types';
import { forkJoin, map, Observable, of, switchMap, tap, throwError } from 'rxjs';
import { StorageService } from './storage.service';
import { sanitizeDescription } from '../utils/text';
import { HttpSyncService } from './http-sync.service';
import { ArticleId } from 'src/rss.types';
import { HttpClient } from '@angular/common/http';

enum TTRssFeedIds {
  all = -4,
  archived = 0,
  labels = -2,
  published = -2,
  stared = -1,
  unread = -3,
}

enum ViewMode {
  all = 'all_articles',
  unread = 'unread',
}

enum TTRssArticleUpdateMode {
  setToFalse = 0,
  setToTrue = 1,
  toggle = 2,
}

enum TTRssUpdatableFields {
  starred = 0,
  published = 1,
  unread = 2,
  note = 3,
}

type TTRSSCategory = {
  id: CategoryId;
  title: string;
};

type TTRSSCounter = {
  id: number;
  counter: number;
};

type TTRSSArticle = {
  author: string;
  feed_id: number;
  feed_title: string;
  id: number;
  unread: boolean;
  marked: boolean;
  link: string;
  title: string;
  content: string;
  updated: number;
};

type QueryPayload = Record<string, string | boolean | number>;

@Injectable({
  providedIn: 'root',
})
export class TTRSSBackendService implements RSSBackend {
  private loggedIn = false;

  constructor(
    private httpSync: HttpSyncService,
    private httpClient: HttpClient,
    private storage: StorageService,
  ) {}

  login(values: Omit<AuthValues, 'token'>): Observable<AuthValues> {
    return this.queryApi<{ session_id: string }>(
      {
        op: 'login',
        user: values.username,
        password: values.password,
      },
      { host: values.host },
    ).pipe(
      switchMap(({ session_id: token }) => {
        if (!token) {
          return throwError(() => new Error('Failed to authenticated.'));
        }

        this.loggedIn = true;

        return of({ ...values, token });
      }),
    );
  }

  logout(): Observable<void> {
    return of(undefined);
  }

  isLoggedIn(): Observable<boolean> {
    if (this.loggedIn) {
      return of(true);
    }

    return this.queryApi<{ status: boolean }>({
      op: 'isLoggedIn',
      sid: this.storage.snapshot.authData.token,
    }).pipe(
      switchMap(({ status }) => {
        if (status) {
          this.loggedIn = true;
          return of(true);
        }

        this.loggedIn = false;
        return of(false);
      }),
    );
  }

  listCategories(): Observable<Category[]> {
    const getCategories = this.queryApi<TTRSSCategory[]>({
      sid: this.storage.snapshot.authData.token,
      op: 'getCategories',
    });
    const getUnreadCounts = this.listCounters();

    return forkJoin({ categories: getCategories, counters: getUnreadCounts }).pipe(
      switchMap(({ categories, counters }) =>
        of(this.updateCategoriesWithCounters(categories, counters)),
      ),
    );
  }

  updateCategoriesWithCounters(
    categories: TTRSSCategory[] | Category[],
    counters: Counter,
  ): Category[] {
    return [
      {
        id: TTRssFeedIds.all.toString(),
        title: 'categories.special.all',
        unreadCount: counters['global-unread'] || 0,
        isSpecial: true,
      },
      {
        id: TTRssFeedIds.unread.toString(),
        title: 'categories.special.unread',
        unreadCount: counters[TTRssFeedIds.unread] || 0,
        isSpecial: true,
      },
      {
        id: TTRssFeedIds.stared.toString(),
        title: 'categories.special.favorites',
        unreadCount: counters[TTRssFeedIds.stared] || 0,
        isSpecial: true,
      },
      ...this.removeSpecialCategories(categories).map((category) => ({
        id: category.id.toString(),
        title: category.title,
        unreadCount: counters[category.id.toString()] || 0,
        isSpecial: false,
      })),
    ];
  }

  private removeSpecialCategories = <T extends { id: CategoryId }>(categories: T[]): T[] => {
    return categories.filter(
      (category) =>
        category.id.toString() !== TTRssFeedIds.all.toString() &&
        category.id.toString() !== TTRssFeedIds.unread.toString() &&
        category.id.toString() !== TTRssFeedIds.stared.toString(),
    );
  };

  listCounters(): Observable<Counter> {
    return this.queryApi<TTRSSCounter[]>({
      sid: this.storage.snapshot.authData.token,
      op: 'getCounters',
    }).pipe(
      map((ttrssCounters) =>
        ttrssCounters.reduce(
          (counters, ttrssCounter) => ({
            ...counters,
            [ttrssCounter.id.toString()]: ttrssCounter.counter,
          }),
          {},
        ),
      ),
    );
  }

  listArticles(rawCategoryId: CategoryId): Observable<Article[]> {
    const categoryId = parseInt(this.transformCategoryUrlIdToCategoryId(rawCategoryId), 10);

    if (!Number.isInteger(categoryId)) {
      return throwError(() => new Error('Invalid category id.'));
    }

    return this.queryApi<TTRSSArticle[]>({
      sid: this.storage.snapshot.authData.token,
      op: 'getHeadlines',
      feed_id: categoryId,
      is_cat: categoryId >= 0,
      show_content: !!this.storage.snapshot.options.fetchDescriptions,
      view_mode: this.storage.snapshot.options.onlyUnread ? ViewMode.unread : ViewMode.all,
    }).pipe(
      map((articles) =>
        articles.map((article) => ({
          id: article.id,
          author: article.author,
          feedId: article.feed_id,
          feedTitle: article.feed_title,
          isFavorite: article.marked,
          isRead: !article.unread,
          link: article.link,
          title: article.title,
          description: sanitizeDescription(article.content),
          updatedAt: new Date(article.updated * 1_000),
        })),
      ),
    );
  }

  markArticlesAsRead(articleIds: ArticleId[]): Observable<void> {
    return this.updateArticle(
      articleIds,
      TTRssUpdatableFields.unread,
      TTRssArticleUpdateMode.setToFalse,
    );
  }

  markArticlesAsUnread(articleIds: ArticleId[]): Observable<void> {
    return this.updateArticle(
      articleIds,
      TTRssUpdatableFields.unread,
      TTRssArticleUpdateMode.setToTrue,
    );
  }

  markArticleAsFavorite(articleId: ArticleId): Observable<void> {
    return this.updateArticle(
      [articleId],
      TTRssUpdatableFields.starred,
      TTRssArticleUpdateMode.setToTrue,
    );
  }

  markArticleAsNotFavorite(articleId: ArticleId): Observable<void> {
    return this.updateArticle(
      [articleId],
      TTRssUpdatableFields.starred,
      TTRssArticleUpdateMode.setToFalse,
    );
  }

  private updateArticle(
    articleIds: ArticleId[],
    field: TTRssUpdatableFields,
    mode: TTRssArticleUpdateMode,
  ): Observable<void> {
    return this.syncApi({
      sid: this.storage.snapshot.authData.token,
      op: 'updateArticle',
      article_ids: articleIds.join(','),
      field,
      mode,
    });
  }

  transformCategoryUrlIdToCategoryId(categoryUrlId: string): CategoryId {
    switch (categoryUrlId.toString().toLowerCase()) {
      case 'all':
        return TTRssFeedIds.all.toString();
      case 'unread':
        return TTRssFeedIds.unread.toString();
      case 'stared':
        return TTRssFeedIds.stared.toString();
      default:
        return categoryUrlId.toString();
    }
  }

  private queryApi<T>(payload: QueryPayload, overriddenAuthData?: { host: string }): Observable<T> {
    const url = this.buildUrl(overriddenAuthData?.host ?? this.storage.snapshot.authData.host);
    return this.httpClient
      .post<{ content: T & { error: string } }>(url, payload)
      .pipe(map((data) => data.content))
      .pipe(
        tap((content) => {
          if (content.error) {
            throw throwError(() => new Error(content.error));
          }
        }),
      );
  }

  private buildUrl(host: string): string {
    const backendUrl = new URL('api/', host);
    return backendUrl.href;
  }

  private syncApi(payload: QueryPayload): Observable<void> {
    if (this.httpSync.mustSync()) {
      const url = this.buildUrl(this.storage.snapshot.authData.host);
      return this.httpSync.post(url, payload);
    }

    return this.queryApi(payload);
  }
}
