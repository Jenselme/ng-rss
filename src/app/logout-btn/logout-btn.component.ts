import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { RSSService } from '../rssservice.service';

@Component({
  selector: 'rss-logout-btn',
  templateUrl: './logout-btn.component.html',
  styleUrls: ['./logout-btn.component.css'],
})
export class LogoutBtnComponent implements OnDestroy {
  private logoutSubscription: Subscription | undefined;
  activatedRouteSubscription: Subscription | undefined;
  onLoginView = false;
  loggingOut = false;

  constructor(
    protected router: Router,
    private rssBackend: RSSService,
  ) {}

  ngOnDestroy() {
    this.logoutSubscription?.unsubscribe();
  }

  logout() {
    this.loggingOut = true;
    this.logoutSubscription?.unsubscribe();
    this.logoutSubscription = this.rssBackend.logout().subscribe(() => {
      this.loggingOut = false;
      this.router.navigate(['/']);
    });
  }
}
