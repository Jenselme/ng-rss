import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'rss-options-dialog',
  templateUrl: './options-dialog.component.html',
  styleUrls: ['./options-dialog.component.css'],
})
export class OptionsDialogComponent {
  constructor(private dialogRef: MatDialogRef<OptionsDialogComponent>) {}

  close() {
    this.dialogRef.close();
  }
}
