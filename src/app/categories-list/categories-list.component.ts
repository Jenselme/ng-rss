import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { RSSService } from '../rssservice.service';
import { Subscription } from 'rxjs';
import { Category, CategoryId } from '../../rss.types';

@Component({
  selector: 'rss-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css'],
})
export class CategoriesListComponent implements OnInit, OnDestroy {
  @Output() categoryClicked = new EventEmitter<Category>();

  private categoriesSubscription: Subscription | undefined;
  categories: Category[] = [];
  loading = false;
  activeCategory: Category | undefined;

  constructor(private rssBackend: RSSService) {}

  ngOnInit() {
    this.categoriesSubscription = this.rssBackend.categories.subscribe(
      ({ activeCategoryId, categories, loading }) => {
        this.activeCategory = categories.find((category) => category.id === activeCategoryId);
        this.categories = categories;
        this.loading = loading;
      },
    );
  }

  ngOnDestroy() {
    this.categoriesSubscription?.unsubscribe();
  }

  trackCategory(index: number, category: Category): CategoryId {
    return category.id;
  }

  handleCategoryClicked(clickedCategory: Category) {
    this.categoryClicked.emit(clickedCategory);
    if (clickedCategory.id !== this.activeCategory?.id) {
      return;
    }

    this.rssBackend.refreshCurrentCategory();
  }
}
