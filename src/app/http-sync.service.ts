import { Inject, Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import Dexie from 'dexie';
import { CONFIG, Config } from 'src/config';
import { SwUpdate } from '@angular/service-worker';

const generateGuid = () =>
  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

interface SyncManager {
  getTags(): Promise<string[]>;
  register(tag: string): Promise<void>;
}

interface ServiceWorkerRegistrationWithSync extends ServiceWorkerRegistration {
  readonly sync: SyncManager;
}

@Injectable({
  providedIn: 'root',
})
export class HttpSyncService {
  private ttrssTable: Dexie.Table;

  constructor(
    @Inject(CONFIG) private config: Config,
    private sw: SwUpdate,
  ) {
    const db = new Dexie(config.swDynamicDbName);
    db.version(config.swDynamicDbSchemaVersion).stores({
      ttrss: 'id,url,payload,failureCounts',
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.ttrssTable = db.ttrss;
  }

  post(url: string, payload: unknown): Observable<void> {
    return from(this.syncPost(url, payload));
  }

  mustSync(): boolean {
    return !navigator.onLine && this.isSWSyncSupported();
  }

  private async syncPost(url: string, payload: unknown): Promise<void> {
    await this.ttrssTable.add({ id: generateGuid(), url, payload, failureCounts: 0 });
    await this.requestSync();
  }

  private isSWSyncSupported(): boolean {
    return 'serviceWorker' in navigator && 'SyncManager' in window && this.sw.isEnabled;
  }

  private async requestSync(): Promise<void> {
    const sw = (await navigator.serviceWorker
      .ready) as unknown as ServiceWorkerRegistrationWithSync;
    await sw.sync.register(this.config.swActionSyncName);
  }
}
