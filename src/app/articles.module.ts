import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ArticlesViewComponent } from './articles-view/articles-view.component';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ReadOnScrollDirective } from './read-on-scroll.directive';
import { Route, RouterModule } from '@angular/router';
import { TranslocoRootModule } from './transloco-root.module';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatListModule } from '@angular/material/list';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { LogoutBtnComponent } from './logout-btn/logout-btn.component';
import { OptionsComponent } from './options/options.component';
import { OptionsDialogComponent } from './options-dialog/options-dialog.component';
import { CategoriesDialogComponent } from './categories-dialog/categories-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CategoryTitleComponent } from './category-title/category-title.component';

const routes: Route[] = [
  {
    path: ':id',
    component: ArticlesViewComponent,
  },
];

@NgModule({
  declarations: [
    ArticlesViewComponent,
    ArticlesListComponent,
    ReadOnScrollDirective,
    CategoriesListComponent,
    LogoutBtnComponent,
    OptionsComponent,
    OptionsDialogComponent,
    CategoriesDialogComponent,
    CategoryTitleComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TranslocoRootModule,
    FlexLayoutModule,
    MatBadgeModule,
    MatListModule,
    MatDialogModule,
    MatMenuModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  exports: [RouterModule],
})
export class ArticlesModule {}
