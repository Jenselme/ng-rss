import { Component, Input } from '@angular/core';
import { Category } from '../../rss.types';

@Component({
  selector: 'rss-category-title',
  templateUrl: './category-title.component.html',
  styleUrls: ['./category-title.component.css'],
})
export class CategoryTitleComponent {
  @Input({ required: true }) category: Category | undefined;
  @Input({ required: false }) active = false;
}
