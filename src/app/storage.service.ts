import { Injectable } from '@angular/core';
import { AuthValues, Options } from '../rss.types';
import { BehaviorSubject, Observable } from 'rxjs';

const authDataKey = 'authData';
const optionsDataKey = 'options';

type SavedData = {
  [authDataKey]?: AuthValues;
  [optionsDataKey]?: Options;
};

export const getInitialAuthData = (): AuthValues => ({
  host: '',
  username: '',
  password: '',
  rssBackend: 'TTRSS',
  token: '',
});

export const getInitialOptions = (): Required<Options> => ({
  fetchDescriptions: true,
  onlyUnread: true,
  readOnScroll: false,
});

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private saveKey = 'ng-rss';
  private readonly authValuesSubject: BehaviorSubject<AuthValues>;
  private readonly optionsSubject: BehaviorSubject<Options>;

  constructor() {
    this.authValuesSubject = new BehaviorSubject<AuthValues>(
      this.loadAuthData() || getInitialAuthData(),
    );
    this.optionsSubject = new BehaviorSubject<Options>(this.loadOptions() || getInitialOptions());
  }

  saveAuthData(authValues: AuthValues): void {
    this.saveData(authDataKey, authValues);
    this.authValuesSubject.next(authValues);
  }

  saveOptions(options: Options): void {
    this.saveData(optionsDataKey, options);
    this.optionsSubject.next(options);
  }

  resetAuthData(): void {
    const authData = getInitialAuthData();
    this.saveData(authDataKey, authData);
    this.authValuesSubject.next(authData);
  }

  public get authValues(): Observable<AuthValues> {
    return this.authValuesSubject.asObservable();
  }

  public get options(): Observable<Options> {
    return this.optionsSubject.asObservable();
  }

  public get snapshot(): Required<SavedData> {
    return {
      [authDataKey]: { ...this.authValuesSubject.value },
      [optionsDataKey]: { ...this.optionsSubject.value },
    };
  }

  private saveData<Key extends keyof SavedData>(key: Key, data: SavedData[Key]): void {
    const dataToSave = this.loadData();
    dataToSave[key] = data;
    localStorage.setItem(this.saveKey, JSON.stringify(dataToSave));
  }

  private loadOptions(): Options {
    return this.loadData()[optionsDataKey] || getInitialOptions();
  }

  private loadAuthData(): AuthValues | undefined {
    return this.loadData()[authDataKey];
  }

  private loadData(): SavedData {
    const loadedData = localStorage.getItem(this.saveKey);
    if (loadedData) {
      return JSON.parse(loadedData);
    }

    return {};
  }
}
