import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { GuardsCheckEnd, GuardsCheckStart, NavigationCancel, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'rss-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  private routerEventsSubscription: Subscription | undefined;
  private networkSnackbarRef: MatSnackBarRef<TextOnlySnackBar> | undefined;
  navigating = false;

  constructor(
    protected router: Router,
    private snackbar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.routerEventsSubscription = this.router.events.subscribe((event) => {
      if (event instanceof GuardsCheckStart) {
        this.navigating = true;
      } else if (event instanceof GuardsCheckEnd || event instanceof NavigationCancel) {
        this.navigating = false;
      }
    });

    if (!navigator.onLine) {
      this.offLine();
    }
  }

  ngOnDestroy() {
    this.routerEventsSubscription?.unsubscribe();
  }

  @HostListener('window:offline')
  offLine() {
    this.networkSnackbarRef = this.snackbar.open('You are offline');
  }

  @HostListener('window:online')
  onOnline() {
    this.networkSnackbarRef?.dismiss();
    this.networkSnackbarRef = undefined;
  }
}
