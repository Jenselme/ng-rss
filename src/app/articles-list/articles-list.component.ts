import { Component, OnDestroy, OnInit } from '@angular/core';
import { RSSService } from '../rssservice.service';
import { Subscription } from 'rxjs';
import { Article, ArticleId } from '../../rss.types';
import { StorageService } from '../storage.service';
import { animate, trigger, transition, style } from '@angular/animations';

@Component({
  selector: 'rss-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css'],
  animations: [
    trigger('deletion', [
      transition(':leave', [
        style({ opacity: '*', height: '*' }),
        animate('0.2s', style({ opacity: 0, height: 0 })),
      ]),
    ]),
  ],
})
export class ArticlesListComponent implements OnInit, OnDestroy {
  private articlesSubscription: Subscription | undefined;
  private optionsSubscription: Subscription | undefined;
  articles: Article[] = [];
  loading = false;
  displayDescriptions = false;
  canReadOnScroll = false;
  slidArticle: Article | undefined;
  slideXCoord = 0;
  swipeCanceled = false;
  maxDescriptionLength = 500;
  showMoreStatusForArticles = new Map<ArticleId, boolean>();

  constructor(
    private rssBackend: RSSService,
    private storage: StorageService,
  ) {}

  ngOnInit() {
    this.articlesSubscription = this.rssBackend.articles.subscribe(({ loading, articles }) => {
      this.loading = loading;
      this.articles = articles;
    });
    this.optionsSubscription = this.storage.options.subscribe((options) => {
      this.displayDescriptions = !!options.fetchDescriptions;
      this.canReadOnScroll = !!options.readOnScroll;
    });
  }

  ngOnDestroy() {
    this.articlesSubscription?.unsubscribe();
    this.optionsSubscription?.unsubscribe();
  }

  trackArticle(index: number, article: Article): ArticleId {
    return article.id;
  }

  showMoreForArticle(article: Article) {
    this.showMoreStatusForArticles.set(article.id, true);
  }

  showLessForArticle(article: Article) {
    this.showMoreStatusForArticles.set(article.id, false);
  }

  markArticleAsRead(articleToUpdate: Article) {
    this.rssBackend.markArticlesAsRead([articleToUpdate]);
  }

  markArticleAsUnread(articleToUpdate: Article) {
    this.rssBackend.markArticlesAsUnread([articleToUpdate]);
  }

  markArticleAsFavorite(articleToUpdate: Article) {
    this.rssBackend.markArticleAsFavorite(articleToUpdate);
  }

  markArticleAsNotFavorite(articleToUpdate: Article) {
    this.rssBackend.markArticleAsNotFavorite(articleToUpdate);
  }

  onPan(event: Event, article: Article) {
    this.swipeCanceled = false;
    this.slidArticle = article;
    const deltaX = (event as unknown as { deltaX: number }).deltaX;
    this.slideXCoord = Math.abs(deltaX) > 25 ? deltaX : 0;
  }

  onSwipe(event: Event, article: Article) {
    if (
      Math.abs(this.slideXCoord) >
      (2 * ((event.target as HTMLElement | null)?.getBoundingClientRect().width || 0)) / 3
    ) {
      this.rssBackend.markArticleAsReadAndDiscard(article);
      this.swipeCanceled = false;
    } else {
      this.cancelSwipe();
    }
  }

  cancelSwipe() {
    this.swipeCanceled = true;
    this.slideXCoord = 0;
  }

  completeSwipe() {
    this.slidArticle = undefined;
    this.slideXCoord = 0;
    this.swipeCanceled = false;
  }
}
