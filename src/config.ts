import { InjectionToken } from '@angular/core';

export type Config = {
  refreshInterval: number;
  swActionSyncName: string;
  swDynamicDbName: string;
  swDynamicDbSchemaVersion: number;
};

export const CONFIG = new InjectionToken('Config', {
  factory: (): Config => {
    return {
      refreshInterval: 30 * 60 * 1000,
      // Keep in sync with sw-sync.js
      swActionSyncName: 'ngrss',
      swDynamicDbName: 'ngrss',
      swDynamicDbSchemaVersion: 1,
    } as const;
  },
});
