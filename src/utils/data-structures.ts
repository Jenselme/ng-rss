import { Subscription } from 'rxjs';

export function* chain<T>(...iterables: Iterable<T>[]) {
  for (const iterable of iterables) {
    for (const value of iterable) {
      yield value;
    }
  }
}

export class ExtendedSet<T> extends Set<T> {
  addAll(...iterables: Iterable<T>[]) {
    for (const value of chain(...iterables)) {
      this.add(value);
    }
  }

  deleteAll(...iterables: Iterable<T>[]) {
    for (const value of chain(...iterables)) {
      this.delete(value);
    }
  }

  union(...iterables: Iterable<T>[]) {
    return new ExtendedSet(chain(this, ...iterables));
  }

  /**
   * Finds the intersection of multiple iterables.
   * Only values present in all of them will be kept.
   *
   * @param {Iterable<T>[]} iterables - The iterables to find the intersection of.
   * @return {ExtendedSet<T>} - The intersection of the iterables.
   */
  intersection(...iterables: Iterable<T>[]) {
    const intersection = new ExtendedSet(this);

    for (const iterable of iterables) {
      const iterableValues = new Set(iterable);
      for (const value of intersection) {
        if (!iterableValues.has(value)) {
          intersection.delete(value);
        }
      }
    }

    return intersection;
  }

  /**
   * Calculate the difference between multiple iterables and return a new ExtendedSet.
   * Only values that are in the current set and not in any other iterables are added.
   *
   * @param {Iterable<T>[]} iterables - The iterables to calculate the difference from.
   * @return {ExtendedSet<T>} - The resulting ExtendedSet containing the difference.
   */
  difference(...iterables: Iterable<T>[]): ExtendedSet<T> {
    const diff = new ExtendedSet<T>(this);

    for (const iterable of iterables) {
      const iterableValues = new Set(iterable);
      for (const value of diff) {
        if (iterableValues.has(value)) {
          diff.delete(value);
        }
      }
    }

    return diff;
  }
}

type KeyType<K> = K extends unknown[] ? string : K;

const convertToImmutableKey = <T>(key: T): KeyType<T> =>
  Array.isArray(key) ? (key.length === 1 ? key[0] : key.join('||')) : key;

export class SubscriptionsMap<T> {
  private map = new Map<KeyType<T>, Subscription>();

  set(key: T, value: Subscription) {
    const keyToUse = convertToImmutableKey(key);
    const existingSubscription: Subscription | undefined = this.map.get(keyToUse);
    existingSubscription?.unsubscribe();
    return this.map.set(keyToUse, value);
  }

  delete(key: T) {
    const keyToUse = convertToImmutableKey(key);
    const existingSubscription: Subscription | undefined = this.map.get(keyToUse);
    existingSubscription?.unsubscribe();
    return this.map.delete(keyToUse);
  }

  clear() {
    for (const subscription of this.map.values()) {
      subscription.unsubscribe();
    }
    return this.map.clear();
  }

  keys() {
    return this.map.keys();
  }

  values() {
    return this.map.values();
  }
}
