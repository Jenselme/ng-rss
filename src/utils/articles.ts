import { BehaviorSubject } from 'rxjs';
import { Article, ArticleId, ArticlesData } from '../rss.types';

export const getArticleIds = (articles: Article[]) => articles.map((article) => article.id);

export class ArticlesSubject {
  private subject = new BehaviorSubject<ArticlesData>({
    articles: [],
    loading: false,
  });

  loading() {
    this.subject.next({
      ...this.subject.value,
      loading: true,
    });
  }

  setArticles(articles: Article[]) {
    this.subject.next({
      ...this.subject.value,
      articles,
      loading: false,
    });
  }

  markAsRead(articleIdsToUpdate: ArticleId[]): void {
    this.subject.next({
      ...this.subject.value,
      articles: this.subject.value.articles.map((article) =>
        articleIdsToUpdate.includes(article.id) ? { ...article, isRead: true } : article,
      ),
    });
  }

  markAsUnread(articleIdsToUpdate: ArticleId[]): void {
    this.subject.next({
      ...this.subject.value,
      articles: this.subject.value.articles.map((article) =>
        articleIdsToUpdate.includes(article.id) ? { ...article, isRead: false } : article,
      ),
    });
  }

  markAsFavorite(articleIdsToUpdate: ArticleId[]): void {
    this.subject.next({
      ...this.subject.value,
      articles: this.subject.value.articles.map((article) =>
        articleIdsToUpdate.includes(article.id) ? { ...article, isFavorite: true } : article,
      ),
    });
  }

  markAsNotFavorite(articleIdsToUpdate: ArticleId[]): void {
    this.subject.next({
      ...this.subject.value,
      articles: this.subject.value.articles.map((article) =>
        articleIdsToUpdate.includes(article.id) ? { ...article, isFavorite: false } : article,
      ),
    });
  }

  discard(articleIdsToDiscard: ArticleId[]): void {
    this.subject.next({
      ...this.subject.value,
      articles: this.subject.value.articles.filter(
        (article) => !articleIdsToDiscard.includes(article.id),
      ),
    });
  }

  asObservable() {
    return this.subject.asObservable();
  }
}
