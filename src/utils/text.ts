const stripHTMLTags = (html: string): string => {
  const doc = new DOMParser().parseFromString(html, 'text/html');
  return doc.body.textContent || '';
};

export const sanitizeDescription = (rawDescription: string | undefined): string => {
  if (!rawDescription) {
    return '';
  }

  return stripHTMLTags(rawDescription);
};
