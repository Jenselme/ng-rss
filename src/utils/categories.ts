import { BehaviorSubject } from 'rxjs';
import { CategoriesData, Category, CategoryId } from 'src/rss.types';

export class CategoriesSubject {
  private subject = new BehaviorSubject<CategoriesData>({
    activeCategoryId: undefined,
    categories: [],
    loading: false,
  });

  get snapshot() {
    return {
      activeCategoryId: this.subject.value.activeCategoryId,
      categories: this.subject.value.categories,
    };
  }

  loading() {
    this.subject.next({
      ...this.subject.value,
      loading: true,
    });
  }

  setCategories(categories: Category[]) {
    this.subject.next({
      ...this.subject.value,
      categories,
      loading: false,
    });
  }

  changeActiveCategory(categoryId: CategoryId) {
    this.subject.next({
      ...this.subject.value,
      activeCategoryId: categoryId,
    });
  }

  asObservable() {
    return this.subject.asObservable();
  }
}
